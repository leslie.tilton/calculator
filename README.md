# Leslie's Notes

Running `npm start` shows the results of the technical challenge within approximately 2 hours.

A calculator with a display shows the current equation until clicking the equals button. After the equals button is clicked, the display shows the result.

There are many improvements I would make given more time. Some are listed below:

- Test cases! I orginally would've liked to add test cases. Extra time was spent to familiarizing myself with chakra since I have not used it in a while.
- Implementing a stack for the equation inputs. This would allow multiple operations to be complete and allow order of operations using a binary tree.
- Adding more comments to explain my thought process. I often add additional comments after making a feature more complete.
- The UI showing the results and equation at the same time
- Equation and result history

Note: There is an algorithm that allows calculations easily to implement the order of operations but I could not remember the name of. I'd highly suggest using this to implement a calculator instead of the simple if statements.

This project was bootstrapped with
[Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br /> Open
[http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br /> You will also see any lint errors
in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br /> See the section
about
[running tests](https://facebook.github.io/create-react-app/docs/running-tests)
for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br /> It correctly bundles
React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br /> Your app is
ready to be deployed!

See the section about
[deployment](https://facebook.github.io/create-react-app/docs/deployment) for
more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can
`eject` at any time. This command will remove the single build dependency from
your project.

Instead, it will copy all the configuration files and the transitive
dependencies (webpack, Babel, ESLint, etc) right into your project so you have
full control over them. All of the commands except `eject` will still work, but
they will point to the copied scripts so you can tweak them. At this point
you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for
small and middle deployments, and you shouldn’t feel obligated to use this
feature. However we understand that this tool wouldn’t be useful if you couldn’t
customize it when you are ready for it.

## Learn More

You can learn more in the
[Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
