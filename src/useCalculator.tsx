import { useState, useEffect } from "react";

type Operation = "+" | "-" | "/" | "x";

const useCalculator = () => {
  const [stack, setStack] = useState<Array<string>>([""]);
  const [operation, setOperation] = useState<Operation | undefined>();
  const [equals, setEquals] = useState<number>(0);

  const typeNumber = (x: string) => {
    let tempStack = [...stack];
    let number = (stack[stack.length - 1] ?? "").concat(x.toString());
    tempStack[stack.length - 1] = number;
    setStack(tempStack);
  };

  useEffect(() => {
    // Calculate the equation if there is more than one item on the stack
    if (stack.length > 1 && !isEmptyString(stack[1])) {
      calculate();
    }
  }, [stack]);

  const clear = () => {
    setStack([""]);
    setOperation(undefined);
    setEquals(0);
  };

  const calculate = () => {
    const num1 = parseFloat(stack[0]);
    const num2 = parseFloat(stack[1]);
    let result = 0;
    if (stack.length == 2) {
      switch (operation) {
        case "+":
          result = num1 + num2;
          break;
        case "-":
          result = num1 - num2;
          break;
        case "/":
          result = num1 / num2;
          break;
        case "x":
          result = num1 * num2;
          break;

        default:
          break;
      }
    }
    // Calculate the result and replace the stack with the result and an empty value to type on
    setStack([result.toPrecision(4)]);
    setEquals(result);
    setOperation(undefined);
  };

  const equation = `${getOrEmptyString(stack[0])} ${getOrEmptyString(
    operation
  )} ${getOrEmptyString(stack[1])}`;

  const handleOperation = (op: Operation) => {
    setStack([...stack, ""]);
    setOperation(op);
  };

  return {
    equals,
    calculate,
    typeNumber,
    operation,
    setOperation: handleOperation,
    clear,
    equation,
  };
};

export default useCalculator;

const getOrEmptyString = (x: string | undefined) => x ?? "";
const isEmptyString = (x: string | undefined) => x == null || x === "";
