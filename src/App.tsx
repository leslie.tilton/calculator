import {
  ChakraProvider,
  Box,
  VStack,
  Grid,
  theme,
  GridItem,
  SimpleGrid,
  Center,
  Button,
  Text,
  Flex,
  Spacer,
} from "@chakra-ui/react";
import useCalculator from "./useCalculator";

const row1 = [7, 8, 9];
const row2 = [4, 5, 6];
const row3 = [1, 2, 3];

export const App = () => {
  const { equals, calculate, equation, typeNumber, setOperation, clear } =
    useCalculator();

  return (
    <ChakraProvider theme={theme}>
      <Box textAlign="center" fontSize="xl">
        <Grid minH="100vh">
          <Center>
            <Grid
              templateAreas={`"header header header"
                  "clear  clear  clear"
                  "numbers numbers operations"
                  "numbers numbers operations"
                  "numbers numbers operations"
                  "numbers numbers operations"`}
              templateColumns="repeat(3, 1fr)"
              templateRows="repeat(6, 1fr)"
              border="1px"
              borderColor="gray.200"
            >
              <GridItem bg="gray.200" area={"header"} p={4}>
                <Flex alignContent={"flex-end"}>
                  <Spacer />
                  <Text>{equation}</Text>
                </Flex>
              </GridItem>
              <GridItem alignItems={"stretch"} area="clear" p="2">
                <Button bg="gray.300" onClick={clear} w="full">
                  C
                </Button>
              </GridItem>
              <GridItem area={"numbers"} pl={4}>
                <SimpleGrid columns={3} gap={4} justifyItems={"center"}>
                  {row1.map((n) => (
                    <Button
                      variant={"outline"}
                      onClick={() => typeNumber(n.toString())}
                    >
                      {n}
                    </Button>
                  ))}
                  {row2.map((n) => (
                    <Button
                      variant={"outline"}
                      onClick={() => typeNumber(n.toString())}
                    >
                      {n}
                    </Button>
                  ))}
                  {row3.map((n) => (
                    <Button
                      variant={"outline"}
                      onClick={() => typeNumber(n.toString())}
                    >
                      {n}
                    </Button>
                  ))}
                  <Button variant={"outline"} onClick={() => typeNumber(".")}>
                    .
                  </Button>
                  <Button variant={"outline"} onClick={() => typeNumber("0")}>
                    {0}
                  </Button>
                  <Button bg="gray.300" onClick={calculate}>
                    =
                  </Button>
                </SimpleGrid>
              </GridItem>
              <GridItem area={"operations"}>
                <VStack gap={2} paddingTop={4}>
                  <Button bg="gray.300" onClick={() => setOperation("/")}>
                    /
                  </Button>
                  <Button bg="gray.300" onClick={() => setOperation("x")}>
                    x
                  </Button>
                  <Button bg="gray.300" onClick={() => setOperation("-")}>
                    -
                  </Button>
                  <Button bg="gray.300" onClick={() => setOperation("+")}>
                    +
                  </Button>
                </VStack>
              </GridItem>
            </Grid>
          </Center>
        </Grid>
      </Box>
    </ChakraProvider>
  );
};
